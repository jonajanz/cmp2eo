
<div class="grid__wrapper  grey">
    <div class="grid__container">
        <div class="grid__row ">
            <!--<li><a href="<?php bloginfo('rss2_url'); ?>">RSS</a></li>-->

                <div class="grid__column-bp1-4">
                    <div id="footer-sidebar" class="secondary">
                        <div id="footer-sidebar1">
                            <?php
                            if(is_active_sidebar('footer-sidebar-1')){
                                dynamic_sidebar('footer-sidebar-1');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="grid__column-bp1-4">
                    <div id="footer-sidebar" class="secondary">
                        <div id="footer-sidebar2">
                        <?php
                        if(is_active_sidebar('footer-sidebar-2')){
                            dynamic_sidebar('footer-sidebar-2');
                        }
                        ?>
                        </div>
                     </div>
                </div>
                <div class="grid__column-bp1-4">
                    <div id="footer-sidebar" class="secondary">
                        <div id="footer-sidebar3">
                            <?php
                            if(is_active_sidebar('footer-sidebar-3')){
                                dynamic_sidebar('footer-sidebar-3');
                            }
                            ?>
                        </div>
                    </div>
                </div>

      </div>
    </div>
</div>
<div class="grid__wrapper ">
    <div class="grid__container">
        <div class="grid__row">
            <div class="footers center white">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>

</body>
</html>
