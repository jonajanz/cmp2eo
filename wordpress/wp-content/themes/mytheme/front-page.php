<?php get_header(); ?>

<div class="grid__wrapper bg">
    <div class="grid__container">
        <div class="grid__row">
            <div class="grid__column-bp1-4 grid__offset-bp1-4">
                <img class=" profielfoto" src="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
                echo $thumb['0']; ?>">
            </div>
        </div>
        <div class="grid__row ">
            <!--<div class="grid__column-bp-1-2 grid__offset-bp1-4 ">-->
            <div class="intro">
                <?php if (have_posts()):?>
                    <?php while (have_posts()): ?>
                        <?php the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="grid__wrapper blauw padding">
    <div class="grid__container">
        <div class="grid__row">
            <div class="grid__column-bp1-4 grid__offset-bp1-4">
                <div class="recent">
                    Recent blogposts
                </div>
            </div>
        </div>

        <div class="grid__row" >
            <?php

            $args = array(
                'posts_per_page' => 3,
                'orderby' => 'most_recent'
            );

            $the_query = new WP_Query( $args );
            ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="grid__column-bp1-4 marges">
                        <div class="container"><img class="picture" src="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
                        echo $thumb['0']; ?>"></div>
                        <div class="witvlak">
                            <div class="blogtitel">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                            </div>
                            <div class="datum">
                                <?php the_time('j F Y'); ?>
                            </div>
                            <div class="tekst">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>

                        <div class="grid__column-bp1-6 grid__offset-bp1-3 readmore"><a href="<?php the_permalink(); ?>" class="readmorea">Read more</a></div>

                    </div>
                <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
            <?php else : ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
