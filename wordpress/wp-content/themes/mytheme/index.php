<?php get_header(); ?>

<div class="grid__wrapper blauw  footer">
    <div class="grid__container">
        <div class="grid__row" >
            <?php if ( have_posts() ) : ?>
                <?php while (have_posts() ) : the_post(); ?>
                    <div class="grid__column-bp1-4 marges">
                        <div class="container"><img class="picture" src="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
                        echo $thumb['0']; ?>"></div>
                        <div class="witvlak">
                            <div class="blogtitel">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                            </div>
                            <div class="datum">
                                <?php the_time('j F Y'); ?>
                            </div>
                            <div class="tekst">
                                 <?php the_excerpt(); ?>
                            </div>
                        </div>

                        <div class="grid__column-bp1-6 grid__offset-bp1-3 readmore"><a href="<?php the_permalink(); ?>" class="readmorea">Read more</a></div>

                    </div>
                <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
            <?php endif; ?>
        </div>
        <div class="grid__row">
            <div class="grid__column-bp1-12">
                <div class="navigation"><p class="prev">&nbsp;<?php previous_posts_link('&laquo; Previous page'); ?></p><p class="next"><?php next_posts_link('Next page &raquo;'); ?>&nbsp;</p></div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
