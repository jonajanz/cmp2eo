<?php get_header(); ?>

<div class="grid__wrapper blauw padding">
    <div class="grid__container">
        <div class="grid__row">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="grid__column-bp1-12 marges">
                        <div class="witbg">
                            <div class="titel">
                                <?php the_title(); ?>
                            </div>
                            <div class="inhoud">
                                <?php the_content(); ?>
                            </div>
                            
                        </div>
                    </div>
                <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
