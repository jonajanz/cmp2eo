<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2eo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O<_(Xwb &B}b9T>.qh$3;DHl`hSNGaG~)Wz.r-<jf7k36*bz/Qcsps9}W7Ac2<8c');
define('SECURE_AUTH_KEY',  '=T.q:)gD7;9_o[9h~W(+.*yE7*X[}rQ.tXoBnW!q,FJ]T_R1.%^xwx+j&YnP8@4F');
define('LOGGED_IN_KEY',    '>k/H^}0Dg-Q4ovOD>E{WYiTGn#6af[jo68~3}CDg5M!GY0R.jXkIH3?7Ul]Zi{.!');
define('NONCE_KEY',        '!)*WQnMEb.MNtJ//R,[(k{DrRo<];7Ds8Nv{n)*]&9aj!kg0yK%{dycjo9BM1{c1');
define('AUTH_SALT',        'IpV5CY_+%4!:LKZQL&(UZ)0@~b,!(f0/,FW5;I*wseyIJtF}R71i=&I1[{( ISW%');
define('SECURE_AUTH_SALT', '5RHf0;QcB^VK>)1P^9}IVw[~1Pl= >!>MPO<A>dLX(|5HI[A7Dlu?tn.3~VEDPbd');
define('LOGGED_IN_SALT',   'l@Os*oUHe&ing_(}Q$oGLH[S<vfP,ia?!sK-&XPxHPrL$*KvDwz9bB4,{V!(+Xx ');
define('NONCE_SALT',       'wH3,bhs{`bX:6fEkm8gjOV$cIa`.(O#bb>;cm[AbP;<pr@R_kB:R-;t`yo&[@eFn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
