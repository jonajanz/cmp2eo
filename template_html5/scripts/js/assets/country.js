//We'll be using an object for each country so we can add data to it
function country(name, id, iso2, lng, lat, score) {
	this.name = name;
	this.id = id;
	this.lng = lng;
	this.lat = lat;
	this.iso2 = iso2;
	this.score = score;
	this.co2emission = null;
	this.disasterRiskReductionProgress = 0;
	this.population = 0;
	this.intentionalHomocides = 0;
	this.childrenInEmploymentTotal = 0;	//All values in a specific timespan
	this.childrenInEmploymentCount = 0;	//The number of values in that specific timespan
	this.childrenInEmployment = function(){	//Will return the average, calculated from the two leading variables
		if (this.childrenInEmploymentCount!=0){
			return this.childrenInEmploymentTotal/this.childrenInEmploymentCount;
		} else {
			return 0;
		}
	}
	this.internetUsers = null;
	this.f_lifeExpectancyTotals = 0;
	this.f_lifeExpectancyTotalsCount = 0;
	this.m_lifeExpectancyTotals = 0;
	this.m_lifeExpectancyTotalsCount = 0;
	this.lifeExpectancyTotals = 0;
	this.lifeExpectancyTotalsCount = 0;
	this.unemploymentTotals = 0;
	this.unemploymentTotalsCount = 0;
	this.f_unemploymentTotals = 0;
	this.f_unemploymentTotalsCount = 0;
	this.m_unemploymentTotals = 0;
	this.m_unemploymentTotalsCount = 0;
	this.businessIndex = null;
	this.businessScore = function(){ //Calculates a score from 0 to 10 from the business index (which goes from 0 to 189)
		if (this.businessIndex != null){
			return 10-(parseFloat(this.businessIndex)/18.9);
		} else {
			return null;
		}
	}
	this.unemployment = function(sex){ //Calculates average unemployment for males/females and for both genders together (transsexual or unspecified)
		if (sex==0){ //Gender unspecified
			if (this.unemploymentTotalsCount!=0){
				return this.unemploymentTotals/this.unemploymentTotalsCount;
			} else {
				return null;
			}
		} else if(sex==1) { //Male
			if (this.m_unemploymentTotalsCount!=0){
				return this.m_unemploymentTotals/this.m_unemploymentTotalsCount;
			} else {
				return null;
			}
		} else { //Female
			if (this.f_unemploymentTotalsCount!=0){
				return this.f_unemploymentTotals/this.f_unemploymentTotalsCount;
			} else {
				return null;
			}
		}
	}
	this.lifeExpectancy = function(sex){ //Calculates average life expectancy for males/females and for both genders together (transsexual or unspecified)
		if (sex==0){
			if (this.lifeExpectancyTotalsCount!=0){
				return this.lifeExpectancyTotals/this.lifeExpectancyTotalsCount;
			} else {
				return null;
			}
		} else if(sex==1) {
			if (this.m_lifeExpectancyTotalsCount!=0){
				return this.m_lifeExpectancyTotals/this.m_lifeExpectancyTotalsCount;
			} else {
				return null;
			}
		} else {
			if (this.f_lifeExpectancyTotalsCount!=0){
				return this.f_lifeExpectancyTotals/this.f_lifeExpectancyTotalsCount;
			} else {
				return null;
			}
		}
	}
	this.fertilityRates = 0;
	this.fertilityRatesCount = 0;
	this.fertilityRate = function(){ //Calculates average fertility rate in a specific timespan
		if (this.fertilityRatesCount!=0){
			return this.fertilityRates/this.fertilityRatesCount;
		} else {
			return null;
		}
	}
	this.mortalityInfants = 0;
	this.mortalityInfantsCount = 0;
	this.mortalityInfant = function(){ //Calculates average mortality of infants (0-14 years old) in a specific timespan
		if (this.mortalityInfantsCount!=0){
			return this.mortalityInfants/this.mortalityInfantsCount;
		} else {
			return null;
		}
	}
	this.dieselPrice = null;
	this.ruralPopulation = null;
	this.medicalBirths = 0;
	this.medicalBirthsCount = 0;
	this.medicalBirth = function(){ //Calculates average percent of births assisted by medical staff in a specific timespan
		if (this.medicalBirthsCount!=0){
			return this.medicalBirths/this.medicalBirthsCount;
		} else {
			return null;
		}
	}
	this.genderEqualityRatings = 0;
	this.genderEqualityRatingsCount = 0;
	this.genderEqualityRating = function(){ //Calculates average gender equality rating in a specific timespan
		if (this.genderEqualityRatingsCount!=0){
			return this.genderEqualityRatings/this.genderEqualityRatingsCount;
		} else {
			return null;
		}
	}
	this.battleRelatedDeaths = 0;
	this.battleRelatedDeathsRate = function(){ //Calculates average rate of battle related deaths in a specific timespan
		if (this.battleRelatedDeaths!=0){
			return this.battleRelatedDeaths/this.population;
		} else {
			return null;
		}
	}
	this.addPoints = function(points){
		//Adds points to score of a country
		this.score += points;
	}
	this.subtractPoints = function(points){
		//Subtracts points from score of a country
		this.score -= points;
	}
}