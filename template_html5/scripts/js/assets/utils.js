var Utils = {

    // Get JSON from localstorage string by his/her namespace
    // Set JSON to localstorage string by his/her namespace
    store: function(namespace, data) {
        if(arguments.length > 1) {
            return localStorage.setItem(namespace, JSON.stringify(data));
        } else {
            var storedData = localStorage.getItem(namespace);
            return (storedData && JSON.parse(storedData)) || null;
        }
    },
    
    sessionStore: function(namespace, data) {
        if(arguments.length > 1) {
            return sessionStorage.setItem(namespace, JSON.stringify(data));
        } else {
            var storedData = sessionStorage.getItem(namespace);
            return (storedData && JSON.parse(storedData)) || null;
        }
    },

    getParamsFromUrl: function(url) {
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
        while(match = regex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    },

    getJSONByPromise: function(url){
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('get', url, true);
            xhr.responseType = 'json';
            xhr.onload = function() {
                if (xhr.status == 200) {
                    var data = (!xhr.responseType)?JSON.parse(xhr.response):xhr.response;
                    resolve(data);
                } else {
                    reject(status);
                }
            };
            xhr.onerror = function() {
                reject(Error("Network Error"));
            };
            xhr.send();
        });
    },

    getJSONPByPromise: function(url) {
        
        var script = document.createElement('script');
        script.src = url;
        
        script.onload = function () {
            this.remove();
        };// After scripts is loaded and executed, remoe it from the DOM 
        
        var head = document.getElementsByTagName('head')[0];
        head.insertBefore(script, head.firstChild);// Insert script into the DOM
        
        var params = this.getParamsFromUrl(url);
        var callbackStr = 'json_callback';
        if(params['prefix']) {
            callbackStr = params['prefix'];
        } else if(params['callback']) {
            callbackStr = params['callback'];
        }
        return new Promise(function(resolve, reject) {
            window[callbackStr] = function(data) {
                resolve(data);
            }
        });
    },

	calculateDistanceBetweenTwoCoordinates: function(lat1, lng1, lat2, lng2){
        var R = 6371; // km
        var lat1 = parseFloat(lat1);
        var lng1 = parseFloat(lng1);
        var lat2 = parseFloat(lat2);
        var lng2 = parseFloat(lng2);

        var dLat = (lat2-lat1).toRad();
        var dLon = (lng2-lng1).toRad();
        var lat1 = lat1.toRad();
        var lat2 = lat2.toRad();

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c
        return d;//in km
    },

    "getDevice": function(){
        if ($(window).width()>(42.5*16)){
            return "";
        } else {
            return "mobile/"
        }
    },

    getGEOLocationByPromise: function(){
        return new Promise(function(resolve, reject) {
            if(Modernizr.geolocation){
                navigator.geolocation.getCurrentPosition(
                    function(position){
                        resolve(position);
                    },
                    function(error){
                        switch(error.code)
                        {
                            case error.PERMISSION_DENIED: console.log("User did not share geolocation data");break;
                            case error.POSITION_UNAVAILABLE: console.log("Could not detect current position");break;
                            case error.TIMEOUT: console.log("Retrieving position timed out");break;
                            default: console.log("Unknown Error");break;
                        }
                        reject(error);
                    },
                    {timeout:10000,enableHighAccuracy:true}
                );
            }
            else{
                reject("HTML5 Geolocation not supported!");
            }
        });
    },

    orientationInit: function(){
        if (this.screenLandscape() && $(window).width()<680){
            this.showOrientMessage(true);
        } else if ((!this.screenLandscape())&&$(window).width()>=680){
            this.showOrientMessage(true);
        } else {
            this.showOrientMessage(false);
        }
    },

    screenLandscape: function(){
        if ($(window).width()>$(window).height())
            return true;
        else
            return false;
    },

    showOrientMessage: function(bool){
        if (bool){
            //Show message
            $('.orientation-lock').removeClass('hidden');
            //Set scroll lock on body
            $('body').addClass('hideScrollBars');
        } else {
            //Hide message
            $('.orientation-lock').addClass('hidden');
            //Remove scroll lock from body
            $('body').removeClass('hideScrollBars');
        }
    },

    toggleFullScreen: function() {
		if (!document.fullscreenElement &&    // alternative standard method
		!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			}
		}
	},

	getTwitterIntent: function(country){
		var text = "Guess what? I'm moving to "+country+"! #whereshouldyoumove? #exodus http://bit.ly/1OeQejh";
		text = encodeURIComponent(text);
		return "https://twitter.com/intent/tweet?text="+text;
	},

	getFacebookIntent: function(country){
		return "https://www.facebook.com/sharer/sharer.php?u=http%3A//bit.ly/1OeQejh";
	}
};