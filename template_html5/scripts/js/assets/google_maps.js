/*
* Load Google Maps Asynchronous
* via appending script
* Don't forget the key: https://console.developers.google.com/flows/enableapi?apiid=maps_backend&keyType=CLIENT_SIDE&reusekey=true&pli=1
* Choose web API
*/
(function(){
    var key = 'AIzaSyDeemkBkliq9Q8rBMCg4xiOYsn95cMUfEo';//Eigen Key Gebruiken!!!

    //Load Google Maps Async
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp'
        + '&key=' + key
        + '&callback=initGoogleMaps';
    document.body.appendChild(script);

    this.initGoogleMaps = function(){
        this._googleMapsInitialized = true;
    };

})();

var GMap = {
    init:function(container) {
        var mapOptions = {
            mapTypeId:'hybrid',
            zoom:5,
            center: new google.maps.LatLng(51.048017, 3.727666)
        };
        this._map = new google.maps.Map(document.querySelector('#' + container), mapOptions);
        google.maps.visualRefresh = true;
        google.maps.event.trigger(this._map, 'resize');
        this._geoLocationMarker = null;
        this._countryMarker = null;
    },
    addMarkerGeoLocation: function(geoLocation) {
        this._geoLocationMarker = new google.maps.Marker({
            position: new google.maps.LatLng(geoLocation.coords.latitude, geoLocation.coords.longitude),
            title:"My location",
            opacity:0.9,
            animation:google.maps.Animation.DROP
        });// Create a Google Maps Marker

        this._geoLocationMarker.setMap(this._map);// Add Marker to Map
    },
    addMarkerCountry: function(name, lat, lng){
        var self = this;

        var marker = null, self = this;
        marker = new google.maps.Marker({
            position:new google.maps.LatLng(lat, lng),
            title:name,
            opacity: 0.9,
            animation:google.maps.Animation.DROP
        });// Create a Google Maps Marker

        marker.setMap(self._map);// Add Marker to Map

        this._map.setCenter(new google.maps.LatLng(lat, lng));// Set center of the map to position
    },

    hideMarkers: function(arrMarkers, hide){
        var self = this;

        _.each(arrMarkers, function(marker){
            if(hide){
                marker.setMap(null);
            }else{
                marker.setMap(self.map);
            }
        });
    },
    refresh: function() {
        google.maps.visualRefresh = true;
        google.maps.event.trigger(this.map,'resize');
    }
};