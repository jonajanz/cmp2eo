<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no"><!--Nodig om responsive te zijn (er goed uit te zien op mobile)-->
    <title>Portfolio Jonas Janzegers </title>
    <meta name="description" content="This is a portfolio website of Jonas Janzegers">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/grid.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>

</head>
<body>
<header>
    <div class="grid__wrapper">
        <div class="grid__container">
            <div class="grid__row">
                <div class=" grid__column-bp1-1 logo">
                    <a href="<?php echo site_url(); ?>"><img src="http://dev-cmp2eo-jonajanz.pantheonsite.io/wp-content/uploads/2016/04/logo.png"></a>
                </div>

                <ul class="grid__column grid__column-bp1-9 grid__offset-bp1-2 nav">

                    <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>


                </ul>
            </div>
        </div>
    </div>

</header>