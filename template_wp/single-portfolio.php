<?php get_header();?>
<div class="grid__wrapper blauw">
    <div class="grid__container">
        <div class="grid__row marges" >
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="grid__column-bp1-12">
                        <div class="witbg">
                            <img class="singlepicture" src="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
                            echo $thumb['0']; ?>">

                        <div class="blogtitel">
                            <?php the_title(); ?>
                        </div>
                        <div class="datum">
                            <?php the_time('j F Y'); ?>
                        </div>
                        <div class="singletekst">
                            <?php the_content(); ?>
                        </div>

                    </div>
        </div>
            <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
            <?php endif; ?>
    </div>
</div>
<div class="grid__wrapper">
    <div class="grid__container"
        <div class="grid__row">
            <div class="grid__column-bp1-12  marges">
                        <?php comments_template(); ?>
            </div>
        </div>
</div>
<div class="grid__wrapper">
    <div class="grid__container forum ">
        <div class="grid__row">
            <div class="grid__column-bp1-12  margesforum">
                <?php echo do_shortcode('[bbp-forum-index]'); ?>
            </div>
        </div>
    </div>
</div>
</div>

<?php get_footer();?>
